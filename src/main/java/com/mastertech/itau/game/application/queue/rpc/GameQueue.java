package com.mastertech.itau.game.application.queue.rpc;

import com.mastertech.itau.game.domain.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Component
public class GameQueue {

    @Value("${com.mastertech.itau.game.application.all-questions-queue}")
    public String allQuestionQueue;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private GameService gameService;

    @JmsListener(destination = "${com.mastertech.itau.game.application.start-game-queue}", containerFactory = "questionJmsFactory")
    public void receiveRequestGameStart(@Payload Map message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
        jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                String gameId = gameService.startNewGame(message.get("playerName").toString(), message.get("gameName").toString(), allQuestionQueue);
                Map<String, String> mapMessage = new HashMap<>();
                mapMessage.put("gameId", gameId);
                Message messageTosend = session.createObjectMessage((Serializable) mapMessage);
                messageTosend.setJMSCorrelationID(jmsMessageHeaderAccessor.getCorrelationId());
                return messageTosend;
            }
        });
    }

    @JmsListener(destination = "${com.mastertech.itau.game.application.all-questions-queue}", containerFactory = "questionJmsFactory")
    public void receiveRequestAllQuestions(@Payload Map message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
        jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                String gameId = gameService.startNewGame(message.get("playerName").toString(), message.get("gameName").toString(), allQuestionQueue);
                Map<String, String> mapMessage = new HashMap<>();
                mapMessage.put("gameId", gameId);
                Message messageTosend = session.createObjectMessage((Serializable) mapMessage);
                messageTosend.setJMSCorrelationID(jmsMessageHeaderAccessor.getCorrelationId());
                return messageTosend;
            }
        });
    }
}
