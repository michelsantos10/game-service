package com.mastertech.itau.game.domain.repository;

import com.mastertech.itau.game.domain.entity.GameState;

public interface GameStateRepository {
    void save(GameState state);
}
