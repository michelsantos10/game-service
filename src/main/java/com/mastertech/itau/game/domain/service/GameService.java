package com.mastertech.itau.game.domain.service;

import com.mastertech.itau.game.domain.entity.GameState;
import com.mastertech.itau.game.domain.entity.Question;
import com.mastertech.itau.game.domain.repository.GameStateRepository;
import com.mastertech.itau.game.domain.repository.QuestionRepository;
import com.mastertech.itau.game.domain.repository.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class GameService {
//    public static Logger logger = (Logger) LoggerFactory.getLogger(GameService.class);

    @Autowired
    public QuestionRepository questionRepository;

    @Autowired
    public RankingRepository rankingRepository;

    @Autowired
    public GameStateRepository gameStateRepository;

    private GameState state;

    @Autowired
    public GameService() {
        this.state = new GameState();
    }

    public String startNewGame(String playerName, String gameName, String queueAllQuestions) throws JMSException {
        this.state = new GameState();
        this.state.setGameId(UUID.randomUUID().toString());
        this.state.setPlayerName(playerName);
        this.state.setQuestions(questionRepository.getAllQuestions(gameName, queueAllQuestions));
        this.state.setCurrentQuestionNumber((short) 0);
        this.state.setStarted(true);
        this.gameStateRepository.save(this.state);
        return this.state.getGameId();
    }

    public boolean isFinished() {
        return this.state.getCurrentQuestionNumber() >= state.getQuestions().size();
    }

    public List<Question> getAllQuestions() {
        return this.state.getQuestions();
    }

    public Short getCurrentQuestionNumber() {
        return this.state.getCurrentQuestionNumber();
    }

    public Map<String, String> getCurrentQuestion() {
        return this.state.getQuestions().get(this.getCurrentQuestionNumber());
    }

    public Short getCorrectAnswerCount() {
        return this.state.getCorrectAnswerCount();
    }

    public void setCorrectAnswerCount(Short correctAnswerCount) {
        this.state.setCorrectAnswerCount(correctAnswerCount);
    }

    public Short getIncorrectAnswerCount() {
        return this.state.getIncorrectAnswerCount();
    }

    public void setIncorrectAnswerCount(Short incorrectAnswerCount) {
        this.state.setIncorrectAnswerCount(incorrectAnswerCount);
    }

    public void goToNextQuestion() {
        if (!this.isFinished()) {
            this.state.setCurrentQuestionNumber((short) (this.state.getCurrentQuestionNumber() + 1));
        }
    }

    public void publishRanking(String queueName) throws JMSException {
        this.rankingRepository.putRanking(this.state, queueName);
    }
}
