package com.mastertech.itau.game.domain.repository;

import com.mastertech.itau.game.domain.entity.GameState;

import javax.jms.JMSException;

public interface RankingRepository {
    public void putRanking(GameState game, String queueName) throws JMSException;
}