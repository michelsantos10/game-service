package com.mastertech.itau.game.domain.entity;

import java.io.Serializable;
import java.util.List;

public class GameState implements Serializable {
    private String playerName;
    private String gameId;
    private boolean started;
    private Short currentQuestionNumber;
    private Short correctAnswerCount;
    private Short incorrectAnswerCount;
    private List<Question> questions;

    public GameState() {
        this.started = false;
        this.currentQuestionNumber = -1;
        this.incorrectAnswerCount = 0;
        this.correctAnswerCount = 0;
//        this.questions = null;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public Short getCurrentQuestionNumber() {
        return currentQuestionNumber;
    }

    public void setCurrentQuestionNumber(Short currentQuestionNumber) {
        this.currentQuestionNumber = currentQuestionNumber;
    }

    public Short getCorrectAnswerCount() {
        return correctAnswerCount;
    }

    public void setCorrectAnswerCount(Short correctAnswerCount) {
        this.correctAnswerCount = correctAnswerCount;
    }

    public Short getIncorrectAnswerCount() {
        return incorrectAnswerCount;
    }

    public void setIncorrectAnswerCount(Short incorrectAnswerCount) {
        this.incorrectAnswerCount = incorrectAnswerCount;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
