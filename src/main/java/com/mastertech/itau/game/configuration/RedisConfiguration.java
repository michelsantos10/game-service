//package com.mastertech.itau.game.configuration;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
//
//@Configuration
//@EnableRedisRepositories
//public class RedisConfiguration {
////    @Bean
////    RedisConnectionFactory redisConnectionFactory() {
////        return new LettuceConnectionFactory();
////    }
////
////    @Bean
////    RedisTemplate<?, ?> redisTemplate(RedisConnectionFactory connectionFactory) {
////        RedisTemplate<byte[], byte[]> template = new RedisTemplate<>();
////        template.setConnectionFactory(connectionFactory);
////        return template;
////    }
//
//
//    @Bean
//    public RedisConnectionFactory redisConnectionFactory() {
//        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
////        jedisConnectionFactory.setHostName("redis-server");
////        jedisConnectionFactory.setPort(7379);
////        jedisConnectionFactory.setPassword("qwerty");
//        return jedisConnectionFactory;
//    }
//
//    @Bean
//    public RedisTemplate<String, Object> redisTemplate() {
//        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
//        template.setConnectionFactory(redisConnectionFactory());
//        return template;
//    }
//}
