package com.mastertech.itau.game.infra.data.repository;

import com.mastertech.itau.game.domain.entity.GameState;
import com.mastertech.itau.game.domain.repository.GameStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

@Repository
public class GameStateRepositoryImpl implements GameStateRepository {
    private static final String KEY = "games-state";
    private RedisTemplate<String, GameState> redisTemplate;
    private HashOperations hashOps;

    @Autowired
    public GameStateRepositoryImpl(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() {
        hashOps = redisTemplate.opsForHash();
    }

    @Override
    public void save(GameState state) {
        hashOps.put(KEY, state.getGameId(), state);
    }
}
